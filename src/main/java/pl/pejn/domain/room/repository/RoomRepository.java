package pl.pejn.domain.room.repository;

import org.springframework.aop.framework.autoproxy.target.LazyInitTargetSourceCreator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.pejn.domain.room.model.Room;

import java.util.List;
import java.util.UUID;

@Repository
public interface RoomRepository extends CrudRepository<Room, UUID> {
    List<Room> findAll();
    Room findOneByName(String name);

}