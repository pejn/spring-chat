package pl.pejn.domain.room.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "This room already exsits")
public class RoomAlreadyExistsException extends RuntimeException {

}
