package pl.pejn.domain.room.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Room {
    @Id
    private  UUID id;
    private  String name;

    private Room() {
    }

    public Room(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
