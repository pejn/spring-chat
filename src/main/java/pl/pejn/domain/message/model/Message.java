package pl.pejn.domain.message.model;

import org.joda.time.DateTime;
import pl.pejn.domain.room.model.Room;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Message {
    @Id
    private  UUID id;

    private  String content;


    @ManyToOne
    private  Room room;
    private  DateTime dateTime;

    private Message() {}

    public Message(String content, Room room) {
        this(UUID.randomUUID(), content, room, DateTime.now());
    }

    public Message(UUID id, String content, Room room, DateTime dateTime) {
        this.id = id;
        this.content = content;
        this.room = room;
        this.dateTime = dateTime;
    }

    public UUID getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public Room getRoom() {
        return room;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

}
