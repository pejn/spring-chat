package pl.pejn.domain.message.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.pejn.domain.message.model.Message;
import pl.pejn.domain.room.model.Room;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessageRepository extends CrudRepository<Message, UUID> {
    List<Message> findAll();
    List<Message> findByRoom(Room room);
}
