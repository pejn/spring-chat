package pl.pejn.services;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pejn.domain.message.model.Message;
import pl.pejn.domain.message.repository.MessageRepository;

import java.util.ArrayList;
import java.util.List;


@Service
public class MessageService {
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<Message> getMessages() {
        return messageRepository.findAll();
    }


    public Message create(String content){
        final Message message = new Message(content, null);
        messageRepository.save(message);
        return message;

    }

    public List<Message> getMessagesSince(DateTime dateTime) {
        final List<Message> messagesSince = new ArrayList<>();
        for (Message message : messageRepository.findAll()) {
            if (message.getDateTime().isAfter(dateTime)) {
                messagesSince.add(message);

            }

        }

        return messagesSince;
    }
}
