package pl.pejn.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pejn.domain.room.exceptions.RoomAlreadyExistsException;
import pl.pejn.domain.room.model.Room;
import pl.pejn.domain.room.repository.RoomRepository;

import java.util.List;
import java.util.UUID;



@Service
public class RoomService {
    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Room getRoomByName(String name){
        return roomRepository.findOneByName(name);
    }

    public List<Room> getRooms(){
        return roomRepository.findAll();
    }

    public Room create(String name) {
        if (getRoomByName(name) != null) {
            throw new RoomAlreadyExistsException();

        }

        final Room room = new Room(UUID.randomUUID(), name);
        roomRepository.save(room);
        return room;
    }
}
