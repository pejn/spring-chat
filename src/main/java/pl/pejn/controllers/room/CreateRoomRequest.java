package pl.pejn.controllers.room;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateRoomRequest {
    private final String name;


    @JsonCreator

    public CreateRoomRequest(
                @JsonProperty(value = "name", required = true) String name
    ) {
        this.name = name;

    }
    public String getName(){
        return name;
    }
}
