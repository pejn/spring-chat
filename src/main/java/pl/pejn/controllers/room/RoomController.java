package pl.pejn.controllers.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.pejn.domain.room.model.Room;
import pl.pejn.services.RoomService;

import java.util.List;

@RestController
@RequestMapping(path = "room")
public class RoomController {
    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Room>> getAll(){
        final List<Room> rooms = roomService.getRooms();
        return ResponseEntity.ok(rooms);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Room> create(@RequestBody CreateRoomRequest request){
        final Room room = roomService.create(request.getName());
        return ResponseEntity.ok(room);

    }



}
