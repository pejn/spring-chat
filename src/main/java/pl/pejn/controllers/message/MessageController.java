package pl.pejn.controllers.message;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pejn.domain.message.model.Message;
import pl.pejn.services.MessageService;

import java.util.List;


@RequestMapping(path = "message")
@RestController
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Message>> getAll(){
        final List<Message> messages = messageService.getMessages();
        return ResponseEntity.ok(messages);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/since/{data}")
    public ResponseEntity<List<Message>> getMessagesSince(@PathVariable("data") String stringDateTime) {
        final DateTime dateTime = new DateTime(Long.valueOf(stringDateTime));
        final List<Message> messages = messageService.getMessagesSince(dateTime);
        return ResponseEntity.ok(messages);


    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Message> create(@RequestBody CreateMessageRequest request){
        final Message message = messageService.create(request.getMessage());
        return ResponseEntity.ok(message);
    }
}
