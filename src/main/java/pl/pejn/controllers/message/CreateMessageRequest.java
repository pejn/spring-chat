package pl.pejn.controllers.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateMessageRequest {
    private final String message;

    @JsonCreator
    public CreateMessageRequest(
            @JsonProperty(value = "message", required = true) String message
    ) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
