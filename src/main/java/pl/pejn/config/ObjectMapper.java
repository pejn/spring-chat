package pl.pejn.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.pejn.utils.JodaTimeSerializer;

@Configuration
public class ObjectMapper {
    @Bean
    public Module getSerializer() {
        final SimpleModule module = new SimpleModule();
        module.addSerializer(new JodaTimeSerializer());

        return module;
    }
}
